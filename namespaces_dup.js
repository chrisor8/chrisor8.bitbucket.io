var namespaces_dup =
[
    [ "BNO055", "namespaceBNO055.html", "namespaceBNO055" ],
    [ "closedloop", "namespaceclosedloop.html", "namespaceclosedloop" ],
    [ "DRV8847", "namespaceDRV8847.html", "namespaceDRV8847" ],
    [ "encoder", "namespaceencoder.html", "namespaceencoder" ],
    [ "HW0x02,03 Submission", "namespaceHW0x02_0003_01Submission.html", null ],
    [ "Lab0x00", "namespaceLab0x00.html", [
      [ "fib", "namespaceLab0x00.html#ac036e4c7f412378e798876aef3c2092f", null ],
      [ "idx", "namespaceLab0x00.html#a82d0a0506a23bb5e8e0676fb7c27f2cc", null ]
    ] ],
    [ "Lab0x01", "namespaceLab0x01.html", [
      [ "onButtonPressFCN", "namespaceLab0x01.html#a30a5cf8034929397cada3568b302d96a", null ],
      [ "ButtonInt", "namespaceLab0x01.html#a7b9c33e3119acd6ca7b401a1fb51bc9f", null ],
      [ "ButtonIsPressed", "namespaceLab0x01.html#ad347235de0d3a7e92e1ae774e94cd72a", null ],
      [ "currentTime", "namespaceLab0x01.html#a20c63a2b162487e190d6c887db631c62", null ],
      [ "pinA5", "namespaceLab0x01.html#a85ea292058f905478f79d7b582f8cac8", null ],
      [ "pinC13", "namespaceLab0x01.html#ab7350683e4e73629aaa40f90ce9b45d9", null ],
      [ "rad", "namespaceLab0x01.html#a44a7bc84d2d83e360d129d76f989f1b2", null ],
      [ "relativeTime", "namespaceLab0x01.html#a53fe12594ee13b1ef18e9abd6307ad74", null ],
      [ "startTime", "namespaceLab0x01.html#a77851af891cc0a0703ce1aeea2be0103", null ],
      [ "state", "namespaceLab0x01.html#af49dfbadfaec711466de0f806232bf66", null ],
      [ "t2ch1", "namespaceLab0x01.html#a3b58c14328a9821ec59cfb491f02bdf9", null ],
      [ "tim2", "namespaceLab0x01.html#a778761870b1d6720e614c945eaa72b9f", null ]
    ] ],
    [ "Lab0x04", "namespaceLab0x04.html", [
      [ "main", "namespaceLab0x04.html#a248c07cea1afebe21965a533c22c3626", null ]
    ] ],
    [ "shares", "namespaceshares.html", "namespaceshares" ],
    [ "task_controller", "namespacetask__controller.html", "namespacetask__controller" ],
    [ "task_encoder", "namespacetask__encoder.html", "namespacetask__encoder" ],
    [ "task_motor", "namespacetask__motor.html", "namespacetask__motor" ],
    [ "task_user", "namespacetask__user.html", "namespacetask__user" ]
];