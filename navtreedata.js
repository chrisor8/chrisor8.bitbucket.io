/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Christopher Or ME305 Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab 0xFF - Term Project", "page1.html", [
      [ "Video Demonstration", "page1.html#sec1", null ],
      [ "Approach", "page1.html#sec2", [
        [ "Task Diagram", "page1.html#subsection1", null ],
        [ "Finite State Machine", "page1.html#subsection2", null ]
      ] ],
      [ "Modeling", "page1.html#sec3", null ],
      [ "Simulation", "page1.html#sec4", null ],
      [ "Determining Gains", "page1.html#sec5", null ],
      [ "Updated Tasks", "page1.html#sec6", null ],
      [ "Important Observations", "page1.html#sec7", null ]
    ] ],
    [ "Lab 0x00 - Fibonacci", "page7.html", [
      [ "Lab 0x00 Source Code", "page7.html#sec20", null ]
    ] ],
    [ "Lab 0x01 - LEDs", "page2.html", [
      [ "Lab 0x01 Source Code", "page2.html#sec10", null ],
      [ "Lab 0x01 Video Demonstration", "page2.html#sec8", null ],
      [ "Lab 0x01 Finite State Machine", "page2.html#sec9", null ]
    ] ],
    [ "Lab 0x02 - Incremental Encoders", "page3.html", [
      [ "Lab 0x02 Source Code", "page3.html#sec11", null ],
      [ "Lab 0x02 Task Diagram", "page3.html#sec12", null ],
      [ "Lab 0x02 Finite State Machine", "page3.html#sec13", null ]
    ] ],
    [ "Lab 0x03 - PMDC Motors", "page4.html", [
      [ "Lab 0x03 Source Code", "page4.html#sec14", null ],
      [ "Lab 0x03 Plots", "page4.html#sec15", null ]
    ] ],
    [ "Lab 0x04 - Closed-Loop Control", "page5.html", [
      [ "Lab 0x04 Source Code", "page5.html#sec16", null ],
      [ "Lab 0x04 Block Diagram and Task Diagram", "page5.html#sec17", null ],
      [ "Lab 0x04 Finite State Machine", "page5.html#sec18", null ]
    ] ],
    [ "Lab 0x05 - IMUs", "page6.html", [
      [ "Lab 0x05 Source Code", "page6.html#sec19", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BNO055_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';